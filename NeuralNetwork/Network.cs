﻿using System;
using System.IO;
using System.Linq;
using System.Text;

namespace NeuralNetwork
{
    public class Network
    {
        public Network(
            int inputsCount, 
            int outputsCount, 
            int firstLayerCount, 
            double learningRate, 
            double learningRateDelta, 
            string trainFile, 
            string testFile,
            int printStep = 5000,
            bool printAccuracy = true)
        {
            InputsCount = inputsCount;
            OutputsCount = outputsCount;
            FirstLayerCount = firstLayerCount;

            LearningRate = learningRate;
            LearningRateDelta = learningRateDelta;
            TrainFile = trainFile;
            TestFile = testFile;

            Inputs = new double[InputsCount];
            Outputs = new double[OutputsCount];
            ExpectedOutputs = new double[OutputsCount];

            FirstLayer = new double[FirstLayerCount];
            FirstLayerWeights = new double[InputsCount, FirstLayerCount];
            FirstLayerBiases = new double[FirstLayerCount];

            OutputLayerBiases = new double[OutputsCount];
            OutputLayerWeights = new double[FirstLayerCount, OutputsCount];

            PrintAccuracy = printAccuracy;
            PrintStep = printStep;
            CorrectlyPredictedInTrainingStepCount = 0;
            CorrectlyPredictedInTestCount = 0;
            TotalTestedImagesCount = 0;

            Initialization();
        }

        public void Train()
        {
            var projectPath = $@"{AppDomain.CurrentDomain.BaseDirectory}\..\..\data\{TrainFile}";
            using (var reader = new StreamReader(projectPath))
            {
                reader.ReadLine();
                string data;
                var count = 0;
                while ((data = reader.ReadLine()) != null)
                {
                    var values = data.Split(',');
                    Label = Convert.ToByte(values[0]);
                    ExpectedOutputs[Label] = 1;
                    for (int i = 1; i <= InputsCount; i++)
                    {
                        Inputs[i - 1] = Convert.ToByte(values[i]);
                    }
                    GeneralTrain();
                    BackPropagation();
                    ClearExpectedOutput();
                    count++;

                    if(PrintAccuracy)
                    {
                        var maxVal = Outputs.Max();
                        var maxIndex = Outputs.ToList().IndexOf(maxVal);
                        if(maxIndex == Label)
                        {
                            CorrectlyPredictedInTrainingStep++;
                        }
                    }

                    if(count % PrintStep == 0)
                    {
                        var accuracy = (double)CorrectlyPredictedInTrainingStep / PrintStep;
                        if (PrintAccuracy)
                        {
                            Console.WriteLine(string.Format("Step: {0},\tAccuracy: {1:P2}.", count, accuracy));
                            CorrectlyPredictedInTrainingStep = 0;
                        }
                        else
                        {
                            Console.WriteLine($"Step: {count}");
                        }
                        
                    }

                }
            }
        }

        public void Test()
        {
            var dataPath = $@"{AppDomain.CurrentDomain.BaseDirectory}\..\..\data\{TestFile}";
            using (var reader = new StreamReader(dataPath))
            {
                reader.ReadLine();
                string data;
                while ((data = reader.ReadLine()) != null)
                {
                    var values = data.Split(',');
                    Label = Convert.ToByte(values[0]);
                    for (int i = 1; i <= InputsCount; i++)
                    {
                        Inputs[i - 1] = Convert.ToByte(values[i]);
                    }
                    GeneralTrain();
                    var maxVal = Outputs.Max();
                    var maxIndex = Outputs.ToList().IndexOf(maxVal);
                    if (maxIndex == Label)
                    {
                        CorrectlyPredictedInTestCount++;
                    }
                    TotalTestedImagesCount++;
                }
                Console.WriteLine($"Correct: {CorrectlyPredictedInTestCount}, from: {TotalTestedImagesCount}");
            }
        }

        private double LearningRateDelta;
        private double LearningRate;
        private string TrainFile;
        private string TestFile;
        private int InputsCount;
        private int OutputsCount;
        private int FirstLayerCount;

        private bool PrintAccuracy;
        private int PrintStep;
        private int CorrectlyPredictedInTrainingStep;
        private int CorrectlyPredictedInTrainingStepCount;
        private int CorrectlyPredictedInTestCount;
        private int TotalTestedImagesCount;

        private double[] Inputs;
        private double[] Outputs;
        private double[] ExpectedOutputs;
        private double[] FirstLayer;
        private double[,] FirstLayerWeights;
        private double[] FirstLayerBiases;
        private double[,] OutputLayerWeights;
        private double[] OutputLayerBiases;
        private int Label;

        private double Sigmoid(double x)
        {
            return 1.0 / (1.0 + Math.Exp(-x));
        }
        private void Initialization()
        {

            double MAX = 1.0 / Math.Sqrt(InputsCount * FirstLayerCount);
            double MIN = -MAX;
            var random = new Random();

            for (int i = 0; i < FirstLayerCount; i++)
            {
                FirstLayerBiases[i] = (random.NextDouble() * (MAX - MIN)) + MIN;
                for (int j = 0; j < InputsCount; j++)
                {
                    FirstLayerWeights[j, i] = (random.NextDouble() * (MAX - MIN)) + MIN;
                }
            }

            MAX = 1.0 / Math.Sqrt(FirstLayerCount * OutputsCount);
            MIN = -MAX;

            for (int i = 0; i < OutputsCount; i++)
            {
                OutputLayerBiases[i] = (random.NextDouble() * (MAX - MIN)) + MIN;
                for (int j = 0; j < FirstLayerCount; j++)
                {
                    OutputLayerWeights[j, i] = (random.NextDouble() * (MAX - MIN)) + MIN;
                }
            }
        }
        private void BackPropagation()
        {
            // Output layer gradient
            for (int i = 0; i < OutputsCount; i++)
            {
                //dC/dOsigmoid(i)
                var dC_Osigmoid = (Outputs[i] - ExpectedOutputs[i]);
                //dOsigmoid(i)/dOnet(i)
                var dOsigm_dOnet = Outputs[i] * (1 - Outputs[i]);

                //dOnet(i)/dBias(i) = 1
                OutputLayerBiases[i] -= (dC_Osigmoid * dOsigm_dOnet) / LearningRate;

                for (int j = 0; j < FirstLayerCount; j++)
                {
                    OutputLayerWeights[j, i] -= (dC_Osigmoid * dOsigm_dOnet * FirstLayer[j]) / LearningRate;
                }
            }

            // First layer gradient
            for (int i = 0; i < FirstLayerCount; i++)
            {
                var dHsigmi_dHneti = FirstLayer[i] * (1 - FirstLayer[i]);
                //var dHneti_dBi = 1;

                // dC/dHsigmj
                double sum = 0.0;
                for (int j = 0; j < OutputsCount; j++)
                {
                    var dCj_dOsigmj = (Outputs[j] - ExpectedOutputs[j]);
                    var dOsigmj_dOnetj = Outputs[j] * (1 - Outputs[j]);
                    var dOnetj_dHsigmi = OutputLayerWeights[i, j];

                    sum += dCj_dOsigmj * dOsigmj_dOnetj * dOnetj_dHsigmi;
                }
                FirstLayerBiases[i] -= (sum * dHsigmi_dHneti) / LearningRate;

                for (int j = 0; j < InputsCount; j++)
                {
                    FirstLayerWeights[j, i] -= (sum * dHsigmi_dHneti * Inputs[j]) / LearningRate;
                }
            }
            LearningRate += LearningRateDelta;
        }
        private void GeneralTrain()
        {
            // Calculation of the first layer
            for (int i = 0; i < FirstLayerCount; i++)
            {
                double x = 0;
                for (int j = 0; j < InputsCount; j++)
                {
                    x += Inputs[j] * FirstLayerWeights[j, i];
                }
                x += FirstLayerBiases[i];
                FirstLayer[i] = Sigmoid(x);
            }
            //Calculation of outputs
            for (int i = 0; i < OutputsCount; i++)
            {
                double x = 0;
                for (int j = 0; j < FirstLayerCount; j++)
                {
                    x += FirstLayer[j] * OutputLayerWeights[j, i];
                }
                x += OutputLayerBiases[i];
                Outputs[i] = Sigmoid(x);
            }
        }
        private void ClearExpectedOutput()
        {
            for (int i = 0; i < OutputsCount; i++)
            {
                ExpectedOutputs[i] = 0;
            }
        }
    }
}
