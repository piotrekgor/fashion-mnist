# **fashion-MNIST**
---
# Public methods
---
    public Network(
            int inputsCount, 
            int outputsCount, 
            int firstLayerCount, 
            double learningRate, 
            double learningRateDelta, 
            string trainFile, 
            string testFile,
            int printStep = 5000,
            bool printAccuracy = true)

*Construct fully-connected neural network*

Arguments:

- **inputsCount**: number of input neurons (input layer size). In this example number of pixels of an image.
- **outputsCount**: number of output neurons (output layer size). In this example number of classes.
- **firstLayerCount**: number of hidden layer neurons.
- **learningRate**: each epoch calculated gradient is divided by this number before subtracting from the model's variable (weight or bias)
- **learningRateDelta**: after each epoch the learning rate will be increased by this value.
- **trainFile**: the name of the train file containing images encoded in csv file.
- **testFile**: the name of the test file containig images encoded in csv file.
- **printStep**: number of epochs to print the progress after. Default value: 5000.
- **printAccuracy**: indicates whether to print current accuracy after certain amount of epochs (specified in the printStep parameter)

---

    public Train()

*Performs training.*

---

    public Test()

*Performs testing.*

---

# Private fields

    private double[] Inputs;

*Holds the input image pixels.*

    private double[] Outputs;

*Holds the outputs i.e. predicted label classification.*

    private double[] ExpectedOutputs;

*Holds the true label classification.*

    private double[] FirstLayer;

*Holds the hidden layer values.*

    private double[,] FirstLayerWeights;

*Holds the first layer weights.*

    private double[] FirstLayerBiases;

*Holds the first layer biases.*

    private double[,] OutputLayerWeights;

*Holds the output layer weights.*

    private double[] OutputLayerBiases;

*Holds the output layer biases.*

    private int Label;

*Holds the current true label.*

    private double Sigmoid(double x)

*Calculates the Sigmoid function for the given x parameter.*

    private void Initialization()

*Performs network initialization, i.e. initializes weights and biases.*

    private void BackPropagation()

*Performs the back propagation, i.e. calculates the gradient descent, divides it by the LearningRate, subtracts from weight/bias and increases LearningRate by LearningRateDelta.*

    private void GeneralTrain()

*Calculates FirstLayer and OutputLayer values.*

    private void ClearExpectedOutput()

*Clears the one-hot-encoded array of true image class after each epoch.*





