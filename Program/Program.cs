﻿using NeuralNetwork;
using System;

namespace Program
{
    class Program
    {
        static void Main(string[] args)
        {
            var network = new Network(
                inputsCount: 784,
                outputsCount: 10,
                firstLayerCount: 300,
                learningRate: 300,
                learningRateDelta: 0.1,
                trainFile: "fashion-mnist_train.csv",
                testFile: "fashion-mnist_test.csv",
                printStep: 5000,
                printAccuracy: true
            );

            Console.WriteLine("Training:");
            network.Train();
            Console.WriteLine("Testing:");
            network.Test();
        }

    }
}
